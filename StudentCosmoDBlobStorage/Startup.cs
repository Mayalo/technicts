﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StudentCosmoDBlobStorage.Startup))]
namespace StudentCosmoDBlobStorage
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ViewModel;
using Business;
namespace StudentCosmoDBlobStorage.Controllers
{
    public class StudentsController : Controller
    {
        CosmodbBussines cosmodb = new CosmodbBussines();
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> StudentsList(string stName)
        {
            List<Student> students = await cosmodb.StudentsList(stName);
            return View(students);
        }

        public ActionResult DeletedView(string stName)
        {
            return View(cosmodb.DeleteStudent(stName));
        }

        public ActionResult AddStudent()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddStudent([Bind(Exclude = "UserPhoto")] Student student)
        {
            HttpPostedFileBase uploadFile = Request.Files["UserPhoto"];
            if (uploadFile.ContentLength != 0)
            {
               await cosmodb.AddStudent(uploadFile,student);
                return RedirectToAction("StudentsList");
            }
            else
            {

                ModelState.AddModelError(string.Empty, "Please Select Picture");

                return View(student);
            }
        }

        public ActionResult EditStudent(string DocumentId)
        {
            return View(cosmodb.EditStudent(DocumentId));
        }

        public async Task<ActionResult> De_Activate(string documentId)
        {
            await cosmodb.Deactivate(documentId);
            return RedirectToAction("StudentsList");
        }

        public async Task<ActionResult> DeLeteStudent(string documentId)
        {
            await cosmodb.DeleteStudent(documentId);
            return RedirectToAction("StudentsList");
        }

        public async Task<ActionResult> UpdateAsync(Student Studentz)
        {
            await cosmodb.UpdateAsync(Studentz);
            return RedirectToAction("StudentsList");
        }
    }
}

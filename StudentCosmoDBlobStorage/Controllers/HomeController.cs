﻿using System.Web.Mvc;
using Business;
using ViewModel;

namespace StudentCosmoDBlobStorage.Controllers
{
    public class HomeController : Controller
    {
        PDFBussiness pdfBussiness = new PDFBussiness();
        EmailBusiness emailBusiness = new EmailBusiness();

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public FileResult CreatePdf()
        {
            var strPDFFileName = pdfBussiness.GetFileName();
            var workStream = pdfBussiness.CreatePdf(strPDFFileName);
            return File(workStream, "application/pdf", strPDFFileName);
        }
        [HttpPost]
        public JsonResult SendPdf()
        {

            var mailObj = new MailModel();

            mailObj.Body = "Hi Please receive student list";
            mailObj.Subject = "Student list pdf";
            mailObj.To = "libongweneo18@gmail.com";

            var strPDFFileName = pdfBussiness.GetFileName();
            var workStream = pdfBussiness.CreatePdf(strPDFFileName);

            emailBusiness.SendEmail(mailObj, workStream, strPDFFileName);

            return new JsonResult { Data = new { status = true, JsonRequestBehavior = JsonRequestBehavior.AllowGet } };
        }


        [HttpGet]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Business;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest
    {
        CosmodbBussines cosmodb = new CosmodbBussines();
        [TestMethod]
        public void GetAllStudentsTest()
        {
            //Arrange
            var count = 0;
            //Actual
            var studentsCount = cosmodb.GetAllStudents().Count;
            //Assert
            Assert.AreNotEqual(count,studentsCount);
        }

        [TestMethod]
        public async System.Threading.Tasks.Task StudentsSearchTestAsync()
        {
            //Arrange
            var count = 0;
            //Actual
            var studentsCount = await  cosmodb.StudentsList("Athi");
            //Assert
            Assert.AreNotEqual(count, studentsCount.Count);
        }
        [TestMethod]
        public async System.Threading.Tasks.Task DeleteStudentsTestAsync()
        {
            //Arrange
            var count = 0;
            //Actual
            var studentsCount = await  cosmodb.DeletedStudentsList("");
            //Assert
            Assert.AreNotEqual(count, studentsCount.Count);
        }
        [TestMethod]
        public async System.Threading.Tasks.Task DeactivateStudentsTestAsync()
        {
            //Arrange
            var count = 0;
            //Actual
            var studentsCount = cosmodb.Deactivate("");
            //Assert
            Assert.AreNotEqual(count, studentsCount);
        }
    }
}

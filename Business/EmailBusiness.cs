﻿
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using ViewModel;

namespace Business
{
   public class EmailBusiness
    {
        private static string senderEmail= ConfigurationManager.AppSettings["GmailAddress"];
        private static string senderPassword= ConfigurationManager.AppSettings["GmailPassword"];

        public void SendEmail(MailModel objModelMail, Stream workStream, string fileName)
        {
            using (MailMessage mail = new MailMessage(senderEmail, objModelMail.To))
              {
                mail.Subject = objModelMail.Subject;
                mail.Body = objModelMail.Body;
                if (workStream != null)
                {
                    mail.Attachments.Add(new Attachment(workStream, fileName));
                }

                mail.IsBodyHtml = false;

                SmtpClient smtp = new SmtpClient("smtp.gmail.com", Convert.ToInt32(587));
                NetworkCredential networkCredential = new NetworkCredential(senderEmail, senderPassword);
                smtp.Credentials = networkCredential;
                smtp.EnableSsl = true;
                smtp.Send(mail);

            }
        }
    }
}

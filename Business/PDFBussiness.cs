﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Business
{
    public class PDFBussiness
    {
        CosmodbBussines cosmodb = new CosmodbBussines();
        
        public string GetFileName() 
        {
            DateTime dTime = DateTime.Now;
            string strPDFFileName = string.Format("SamplePdf" + dTime.ToString("yyyyMMdd") + "-" + ".pdf");
            return strPDFFileName;
        }

        public MemoryStream CreatePdf(string strPDFFileName)
        {
            MemoryStream workStream = new MemoryStream();
            StringBuilder status = new StringBuilder("");

            iTextSharp.text.Document doc = new iTextSharp.text.Document();
            doc.SetMargins(0f, 0f, 0f, 0f);
            //Create PDF Table with 5 columns
            PdfPTable tableLayout = new PdfPTable(6);
            doc.SetMargins(0f, 0f, 0f, 0f);

            //Create PDF Table
            PdfWriter.GetInstance(doc, workStream).CloseStream = false;
            doc.Open();

            //Add Content to PDF 
            doc.Add(Add_Content_To_PDF(tableLayout));

            // Closing the document
            doc.Close();
            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;
            return workStream;
        }

        protected PdfPTable Add_Content_To_PDF(PdfPTable tableLayout)
        {
            float[] headers = { 35, 35, 35, 35, 35, 35 };  //Header Widths
            tableLayout.SetWidths(headers);        //Set the pdf headers
            tableLayout.WidthPercentage = 100;       //Set the PDF File witdh percentage
            tableLayout.HeaderRows = 1;

            //Add Title to the PDF file at the top

            tableLayout.AddCell(new PdfPCell(new Phrase("DUT Students List", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(0, 0, 0)))) { Colspan = 12, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });
            
            ////Add header
           
            AddCellToHeader(tableLayout, "Student Number");
            AddCellToHeader(tableLayout, "Name");
            AddCellToHeader(tableLayout, "Surname");
            AddCellToHeader(tableLayout, "Email Address");
            AddCellToHeader(tableLayout, "Telephone Number");
            AddCellToHeader(tableLayout, "Mobile Number");

            ////Add body
            var studentQuery = cosmodb.GetAllStudents();

            foreach (var item in studentQuery.ToList())
            {
                AddCellToBody(tableLayout, item.studentNumber.ToString());
                AddCellToBody(tableLayout, item.studentName);
                AddCellToBody(tableLayout, item.surname);
                AddCellToBody(tableLayout, item.email);
                AddCellToBody(tableLayout, item.telphone_No);
                AddCellToBody(tableLayout, item.mobile);
            }



            return tableLayout;
        }

        // Method to add single cell to the Header
        private static void AddCellToHeader(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, iTextSharp.text.BaseColor.YELLOW))) { HorizontalAlignment = Element.ALIGN_LEFT, Padding = 5, BackgroundColor = new iTextSharp.text.BaseColor(128, 0, 0) });
        }

        // Method to add single cell to the body
        private static void AddCellToBody(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, iTextSharp.text.BaseColor.BLACK))) { HorizontalAlignment = Element.ALIGN_LEFT, Padding = 5, BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255) });
        }
    }
}

